FROM alpine

LABEL author=gmayer@arkho.tech

# Download hugo
ENV HUGO_VERSION 0.42.1
ADD https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/hugo_${HUGO_VERSION}_Linux-64bit.tar.gz /tmp/hugo.tar.gz

# Unzip hugo and move it to /usr/local/bin
RUN cd /tmp && \
    tar -xzf hugo.tar.gz && \
    mv /tmp/hugo /usr/local/bin/hugo && \
    rm /tmp/*

# Create working directory
RUN mkdir /site
WORKDIR /site



