# Docker Image for Hugo static site generator

Hugo v0.42.1

## Usage

This image is for using in multi-stage build, like this for example:

```Dockerfile
FROM gui77aume/hugo AS builder
COPY . /site
RUN hugo

FROM nginx:alpine
COPY --from=builder /site/public /usr/share/nginx/html
```

It is possible to use it for Gitlab Pages too, configuring your .gitlab-ci.yml file like this:

```yaml
image: gui77aume/hugo

# You can skip that if you are not using git submodules
variables:
  GIT_SUBMODULE_STRATEGY: recursive

pages:
  script:
  - hugo
  artifacts:
    paths:
    - public
  only:
  - master
```
